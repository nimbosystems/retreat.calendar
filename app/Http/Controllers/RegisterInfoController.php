<?php

namespace App\Http\Controllers;

use App\RegisterInfo;
use Illuminate\Http\Request;

class RegisterInfoController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  $register
     * @return \Illuminate\Http\Response
     */
    public function show($register)
    {
        $info = RegisterInfo::find($register);
        if(!$info){
            $info = RegisterInfo::make(['id' => $register]);
        }
        return $info;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $register
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $register)
    {
        $info = RegisterInfo::find($register);
        if($info){
            $info->update($request->all());
        }else{
            $info = RegisterInfo::create($request->all());
        }
        return $info;
    }


}
