<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterInfo extends Model
{
    protected $table = 'register_info';
    protected $guarded = [];

}
