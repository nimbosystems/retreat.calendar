<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegisterInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_info', function (Blueprint $table) {
            $table->unsignedInteger('id')->primary();
            $table->text('flight_info')->nullable();
            $table->string('meal_preference')->nullable();
            $table->boolean('yoga_class')->default(false);
            $table->boolean('juice_detox')->default(false);
            $table->boolean('massage')->default(false);
            $table->boolean('breath_work')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_info');
    }
}
