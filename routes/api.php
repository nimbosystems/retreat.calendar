<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/registrations', function (Request $request) {
    $params = ($request->all());
    $params['token'] = 'ef061e1a717568ee5ca5c76a94cf5842';
    $result = file_get_contents("https://demo14.secure.retreat.guru/api/v1/registrations?".http_build_query($params));
    $registrations = json_decode($result);

    return $registrations;
});

Route::put('/info/{register}', 'RegisterInfoController@update');
Route::get('/info/{register}', 'RegisterInfoController@show');

